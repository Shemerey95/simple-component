<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * @var string $componentPath
 * @var string $componentName
 * @var array $arCurrentValues
 * */

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if( !Loader::includeModule("iblock") ) {
    throw new \Exception('Не загружены модули необходимые для работы компонента');
}

// типы инфоблоков
$arIBlockType = CIBlockParameters::GetIBlockTypes();

// инфоблоки выбранного типа
$arIBlock = [];
$iblockFilter = !empty($arCurrentValues['IBLOCK_TYPE'])
    ? ['TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE' => 'Y']
    : ['ACTIVE' => 'Y'];

$rsIBlock = CIBlock::GetList(['SORT' => 'ASC'], $iblockFilter);
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];
}
unset($arr, $rsIBlock, $iblockFilter);

// раздел инфоблока
$str = preg_replace("/[^0-9]/", '', $arIBlock);
$sectionList = CIBlockSection::GetList(
        ['name' => 'asc'],
        [
            'IBLOCK_ID'  => $str,
        ]
    );
    while ($parentSection = $sectionList->GetNext())
    {
        $iSection[$parentSection["ID"]] = '[' . $parentSection["ID"] . '] ' . $parentSection["NAME"];
    }

$arComponentParameters = [
    "GROUPS" => [
        "SETTINGS" => [
            "NAME" => Loc::getMessage('EXAMPLE_COMPSIMPLE_PROP_SETTINGS'),
            "SORT" => 550,
        ],
        "SIZE" => [
            "NAME" => Loc::getMessage('EXAMPLE_COMPSIMPLE_PROP_SIZE'),
            "SORT" => 560,
        ],
    ],

    "PARAMETERS" => [
        "IBLOCK_TYPE" => [
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage('EXAMPLE_COMPSIMPLE_PROP_IBLOCK_TYPE'),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y"
        ],
        "IBLOCK_ID" => [
            "PARENT" => "SETTINGS",
            "NAME" => Loc::getMessage('EXAMPLE_COMPSIMPLE_PROP_IBLOCK_ID'),
            "TYPE" => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y"
        ],
        "SIZE_HEIGHT" => [
            "PARENT" => "SIZE",
            "NAME" => Loc::getMessage('EXAMPLE_COMPSIMPLE_PROP_SECTION_HEIGHT'),
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "",
            "COLS" => 25
        ],
        "SIZE_WIDTH" => [
            "PARENT" => "SIZE",
            "NAME" => Loc::getMessage('EXAMPLE_COMPSIMPLE_PROP_SECTION_WIDTH'),
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "",
            "COLS" => 25
        ],
        'CACHE_TIME' => ['DEFAULT' => 3600],
    ]
];