<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("linux-soft");
?>

<?//Список элементов?>
<?$APPLICATION->IncludeComponent(
	"custom:custom.element.list",
	".default",
	array(
		"IBLOCK_ID" => "4",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "linux-soft",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"SIZE_HEIGHT" => "200",
		"SIZE_WIDTH" => "200",
		"SECTION_ID" => "17"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
