<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Loader,
    \Bitrix\Main\Application;

class ExampleCompSimple extends CBitrixComponent
{
    public  $arItem = [];
    private $_request;
    
    /**
     * Проверка наличия модулей, требуемых для работы компонента
     * @throws Exception
     * @return bool
     */
    private function _checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new \Exception('Не загружены модули необходимые для работы модуля');
        }
        
        return true;
    }
    
    /**
     * Подготовка параметров компонента
     *
     * @param $arParams
     *
     * @return mixed
     */
    public function onPrepareComponentParams($arParams)
    {
        
        // тут пишем логику обработки параметров, дополнение параметрами по умолчанию
        // и прочие нужные вещи
        
        return $arParams;
    }
    
    /**
     * Получение элементов инфоблока
     *
     * @param $arParams
     *
     * @return string
     */
    public function getElements()
    {
        $arSelect = ["ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "PREVIEW_TEXT"];
        $arFilter = [
            // "SECTION_ID" => $this->arParams["SECTION_ID"],
            "IBLOCK_ID"  => $this->arParams["IBLOCK_ID"],
            "IBLOCK_LID" => SITE_ID,
            "ACTIVE"     => "Y",
        ];
        
        $elements = CIBlockElement::GetList(
            ["ID" => "ASC"],
            $arFilter,
            false,
            false,
            $arSelect
        );
        
        while ($arItems = $elements->GetNextElement()) {
            $arItem                        = $arItems->GetFields();
            $arItem["PREVIEW_PICTURE_SRC"] = CFile::GetPath($arItem["PREVIEW_PICTURE"]);
            $arResult["ITEMS"][]           = $arItem;
        }
        
        $this->arResult = $arResult;
    }
    
    /**
     * Ресайз изображений
     *
     * @param $arParams
     *
     * @return string
     */
    public function resizeImage()
    {
        foreach ($this->arResult["ITEMS"] as $elements) {
            $fileSrc                               = CFile::ResizeImageGet($elements["PREVIEW_PICTURE"],
                ["width" => $this->arParams["SIZE_WIDTH"], "height" => $this->arParams["SIZE_HEIGHT"], BX_RESIZE_IMAGE_PROPORTIONAL, true]);
            $elements["PREVIEW_PICTURE_SRC_SMALL"] = $fileSrc['src'];
            $arResult["ITEMS"][]                   = $elements;
        }
        
        $this->arResult = $arResult;
    }
    
    /**
     * Запуск компонента
     * @throws \Bitrix\Main\SystemException
     * @return mixed|void
     */
    public function executeComponent()
    {
        $this->_checkModules();
        
        $this->_request = Application::getInstance()->getContext()->getRequest();
        
        $this->getElements();
        $this->resizeImage();
        
        $this->includeComponentTemplate();
    }
}